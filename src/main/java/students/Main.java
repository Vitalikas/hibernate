package students;

import org.hibernate.Session;
import org.hibernate.Transaction;
import students.entity.Course;
import students.entity.Details;
import students.entity.Student;
import students.entity.University;
import students.util.HibernateUtil;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

//            // creating universities and students and setting universities to students
//            University university1 = new University("KU");
//            University university2 = new University("VU");
//
//            Student student1 = new Student("Vitalij", "vit@vit.lt", 58);
//            Student student2 = new Student("Andrius", "and@and.lt", 34);
//            Student student3 = new Student("Jevgenij", "jev@jev.lt", 21);
//
//            student1.setUniversity(university1);
//            student2.setUniversity(university2);
//            student3.setUniversity(university2);
//
//            // creating courses and setting students to courses
//            Course course1 = new Course("Java");
//            Course course2 = new Course("Python");
//            Course course3 = new Course("C#");
//
//            Set<Student> studentSet1 = new HashSet<>();
//            studentSet1.add(student1);
//            studentSet1.add(student2);
//            Set<Student> studentSet2 = new HashSet<>();
//            studentSet2.add(student1);
//            studentSet2.add(student3);
//            Set<Student> studentSet3 = new HashSet<>();
//            studentSet3.add(student1);
//            studentSet3.add(student3);
//
//            course1.setStudents(studentSet1);
//            course2.setStudents(studentSet2);
//            course3.setStudents(studentSet3);
//
//            // creating details and setting courses to details
//            Details details1 = new Details(1200, 6);
//            Details details2 = new Details(1800, 9);
//            Details details3 = new Details(400, 2);
//
//            details1.setCourse(course1);
//            details2.setCourse(course2);
//            details3.setCourse(course3);
//
//            session.save(details1);
//            session.save(details2);
//            session.save(details3);

            Student student = session.createQuery("SELECT s FROM Student s WHERE s.name='Vitalij'", Student.class)
                    .getSingleResult();
            // Student stud = session.get(Student.class, 1L);

            // committing
//            transaction.commit();

            AtomicInteger count = new AtomicInteger(1);
            System.out.println(student);
            System.out.println(student.getName() + " has courses: ");
            Set<Course> courses = student.getCourses();
            courses.forEach(course -> {
                System.out.println(count + ". " + course.getName());
                System.out.println(course.getDetails());
                System.out.println("This course belongs to: " + course.getStudents());
                count.getAndIncrement();
            });
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                e.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        // shutting down
        HibernateUtil.shutdown();
    }
}