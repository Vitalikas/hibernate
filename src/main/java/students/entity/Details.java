package students.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Table(name = "course_details")
@Getter
@Setter
public class Details {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int price;
    private int duration;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    private Course course;

    public Details() {
    }

    public Details(int price, int duration) {
        this.price = price;
        this.duration = duration;
    }

    @Override
    public String toString() {
        return String.format("details [id = %d, price = %d, duration = %d]", id, price, duration);
    }
}