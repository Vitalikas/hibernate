package students.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "students")
@Getter
@Setter
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(unique = true)
    private String email;
    private int age;

    @ManyToMany(mappedBy = "students", cascade = CascadeType.ALL)
    private Set<Course> courses;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "university_id")
    private University university;

    public Student() {
    }

    public Student(String name, String email, int age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Student [id = %d, name = %s, email = %s, age = %d, course count = %d, university = %s]",
                id,
                name,
                email,
                age,
                courses.size(),
                university.getName());
    }
}